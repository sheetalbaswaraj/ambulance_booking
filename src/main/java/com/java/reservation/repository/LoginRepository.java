package com.java.reservation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.java.reservation.entity.Registration;

public interface LoginRepository extends JpaRepository<Registration, Long> {

	   @Query("from Registration where mobileno=?1")
	   Registration gettingBmCred(long mobileno);
}
