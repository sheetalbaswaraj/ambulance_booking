package com.java.reservation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.java.reservation.entity.PatientDetails;

@Repository
public interface PatientRepository extends JpaRepository<PatientDetails, String> {
	
}
