package com.java.reservation.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.java.reservation.entity.ShowtimeEntity;


public interface TimingRepository extends JpaRepository<ShowtimeEntity, String> {
	

}
