package com.java.reservation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.java.reservation.entity.Otpdata;


public interface OTPRepository extends JpaRepository<Otpdata, Long> {
	@Query("from Otpdata where mobileno=?1")
	Otpdata getOtp(long mobileno);


}
