package com.java.reservation.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.reservation.entity.Registration;
import com.java.reservation.repository.RegistrationRepository;
import com.java.reservation.util.Response;
import com.java.reservation.util.TripleDES;

@Service
public class RegistrationService {

	@Autowired

	private RegistrationRepository registrationRepository;

	public Registration updateOrsave(Registration registerform) throws Exception {
		TripleDES tripleDES = new TripleDES();
		String password = registerform.getPassword();
		String encrypt = tripleDES.encrypt(password);
		Optional<Registration> registerData = registrationRepository.findById(registerform.getMobileno());
		if (registerData.isPresent()) {
			Registration register = registerData.get();
			register.setMobileno(registerform.getMobileno());
			register.setPassword(encrypt);
			Registration data = registrationRepository.save(register);
			return data;

		} else {
			registerform.setMobileno(registerform.getMobileno());
			registerform.setPassword(encrypt);
			registerform = registrationRepository.save(registerform);
			return registerform;
		}
	}

	public Response deleteId(long mobileno) {
		Response response = new Response();
		Optional<Registration> register = registrationRepository.findById(mobileno);
		if (register.isPresent()) {
			registrationRepository.deleteById(mobileno);
			response.setStatus(00);
			response.setMessage("The Registered mobile no is deleted");
		} else {
			response.setStatus(01);
			response.setMessage("The  mobile no is not registered");

		}
		return response;
	}

}
