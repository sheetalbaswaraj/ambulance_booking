package com.java.reservation.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.reservation.entity.PatientDetails;
import com.java.reservation.repository.PatientRepository;
import com.java.reservation.util.Response;

@Service
public class PatientDetailService {
	@Autowired
	private PatientRepository patientRepository;

	public PatientDetails updateOrsave(PatientDetails patientdetail) throws Exception {
		System.out.println(patientdetail.getPatientname());
		Optional<PatientDetails> PatientData = patientRepository.findById(patientdetail.getPatientname());
		if (PatientData.isPresent()) {
			PatientDetails patientRegister = PatientData.get();
			patientRegister.setAddress(patientdetail.getAddress());
			patientRegister.setLongitude(patientdetail.getLongitude());
			patientRegister.setLatitude(patientdetail.getLatitude());
			patientRegister.setAddress(patientdetail.getAddress());
			PatientDetails data = patientRepository.save(patientRegister);
			return data;

		} else {

			PatientDetails patientdata = patientRepository.save(patientdetail);
			return patientdata;
		}

	}
	public Response deleteId(String patientname) {
		Response response = new Response();
		Optional<PatientDetails> patient = patientRepository.findById(patientname);
		if(patient.isPresent()) {
			patientRepository.deleteById(patientname);
			response.setStatus(00);
			response.setMessage("THe patient is Deleted");
		}else {
			response.setStatus(01);
			response.setMessage("THe patient is not avaliable");
			
		}
		return response;
	}
}
