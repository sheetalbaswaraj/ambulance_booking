package com.java.reservation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.reservation.entity.ShowtimeEntity;
import com.java.reservation.repository.TimingRepository;
import com.java.reservation.util.Response;

import RecordNotFoundException.RecordNotFoundException;

@Service
public class ShowtimeService {
	@Autowired
	private TimingRepository timingRepository;

	public Response saveData(ShowtimeEntity timedata) {
		Response response = new Response();
		ShowtimeEntity timeData2 = new ShowtimeEntity();
		if (timedata != null) {
			timeData2.setTime(timedata.getTime());
			timeData2.setDate(timedata.getDate());
			timeData2.setDistance(timedata.getDistance());
			timeData2.setFare(timedata.getFare());

			timingRepository.save(timeData2);
			response.setStatus(00);
			response.setMessage("The timing has been saved");
		} else {
			response.setStatus(01);
			response.setMessage("The timing has  not been saved");
		}
		return response;
	}

	public ShowtimeEntity getdata(String  latitude , String longitude) throws RecordNotFoundException {
		ShowtimeEntity showtimeEntity = new ShowtimeEntity();
		if (latitude!=null && longitude!=null) {
			showtimeEntity.setLocation("Hyderabad");
			showtimeEntity.setLatitude(latitude);
			showtimeEntity.setLongitude(longitude);
			showtimeEntity.setTime("20.00hrs");
			showtimeEntity.setLocation("Hyderabad");
			showtimeEntity.setDate("01/01/2021");
			showtimeEntity.setDistance("21km");
			showtimeEntity.setFare("200Rs");
			return showtimeEntity;

		} else {
			throw new RecordNotFoundException("The location is not correct try again");
		}

	}
}
