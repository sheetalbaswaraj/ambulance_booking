package com.java.reservation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.reservation.dao.LoginDao;
import com.java.reservation.entity.Registration;
import com.java.reservation.util.Response;
import com.java.reservation.util.TripleDES;

@Service
public class LoginService {

	@Autowired
	private LoginDao loginDao;

	public Response fetchLoginDetailes(long mobileno, String password) {
		System.out.println(mobileno+password);
		Registration ss = getByMobileno(mobileno);
		Response reponse = new Response();
		System.out.println(ss.getPassword());
		try {
			if (ss != null) {
				TripleDES des = new TripleDES();
				String encryptpassword = des.encrypt(password);
				if (mobileno!=0l && encryptpassword.equals(ss.getPassword())) {
					reponse.setStatus(00);
					reponse.setMessage("The Credentials has been verified");
				} else {
					reponse.setStatus(01);
					reponse.setMessage("The Credentials are not verified");
				}

			} else {
				reponse.setStatus(04);
				reponse.setMessage("No such Data has been found");
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		return reponse;
	}


	public Registration getByMobileno(long mobileno) {
		return loginDao.verifyCredentials(mobileno);
	}
}
