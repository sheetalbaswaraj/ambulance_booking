package com.java.reservation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.reservation.dao.OTPDao;
import com.java.reservation.entity.Otpdata;
import com.java.reservation.repository.OTPRepository;
import com.java.reservation.util.Response;

@Service
public class OTPService {
	@Autowired
	private OTPRepository otpRepository;
	@Autowired
	private OTPDao otpDao;

	public Response saveData(Otpdata otpverificationData) {
		Response response = new Response();
		Otpdata otPverificationData2 = new Otpdata();
		if (otpverificationData != null) {
			otPverificationData2.setMobileno(otpverificationData.getMobileno());
			otPverificationData2.setOtp(otpverificationData.getOtp());
			otPverificationData2.setSessionkey(otpverificationData.getSessionkey());
			otpRepository.save(otPverificationData2);
			response.setStatus(00);
			response.setMessage("The otp has been saved");
		} else {
			response.setStatus(01);
			response.setMessage("The otp has been not been saved");
		}
		return response;
	}

	public Otpdata verifyOTP(long mobileno) {
		Otpdata verifyOTP = otpDao.verifyOTP(mobileno);
		return verifyOTP;
	}

	@SuppressWarnings("unused")
	public Response fetchOTPdetails(long mobileno, String otp, String sessionkey) {
		Otpdata ss = verifyOTP(mobileno);
		Response reponse = new Response();
		try {
			if (ss != null) {
				String otp2 = ss.getOtp();
				long mobileno2 = ss.getMobileno();
				String sessionkey2 = ss.getSessionkey();
				if (mobileno != 0l && otp.equals(otp2) && sessionkey.equals(sessionkey2)) {
					reponse.setStatus(00);
					reponse.setMessage("The OTP has been verified");
				} else {
					reponse.setStatus(01);
					reponse.setMessage("The OTP is not verified");
				}

			} else {
				reponse.setStatus(04);
				reponse.setMessage("No such Data has been found");
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		return reponse;
	}

	public List<Otpdata> getOTPList() {
		List<Otpdata> listData = otpRepository.findAll();
		return listData;
	}
}
