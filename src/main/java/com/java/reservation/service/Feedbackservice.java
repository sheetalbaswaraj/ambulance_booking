package com.java.reservation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.reservation.entity.Feedbackformentity;

import com.java.reservation.repository.feedbackrepository;
import com.java.reservation.util.Response;

@Service
public class Feedbackservice {
	@Autowired
	private feedbackrepository feedbackRepository;

	public Response saveData(Feedbackformentity feedbackdata) {
		Response response = new Response();
		if (feedbackdata != null) {
			Feedbackformentity feedbackformentity = new Feedbackformentity();
			feedbackformentity.setCustomermobileno(feedbackdata.getCustomermobileno());
			feedbackformentity.setCustomername(feedbackdata.getCustomername());
			feedbackformentity.setCustomercomments(feedbackdata.getCustomercomments());
			feedbackformentity.setRating(feedbackdata.getRating());

			feedbackRepository.save(feedbackformentity);
			response.setStatus(00);
			response.setMessage("The feedback has been saved");
		} else {
			response.setStatus(01);
			response.setMessage("The feedback has  not been saved");
		}
		return response;
	}
}
