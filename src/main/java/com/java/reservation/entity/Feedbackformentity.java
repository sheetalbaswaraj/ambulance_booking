package com.java.reservation.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "feedback_Table1")
public class Feedbackformentity {

	@Id
	@Column(name = "customermobileno")
	private long customermobileno;
	@Column(name = "customername")
	private String customername;
	@Column(name = "customercomments")
	private String customercomments;
	@Column(name = "rating")
	private String rating;

	public String getCustomername() {
		return customername;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}

	public long getCustomermobileno() {
		return customermobileno;
	}

	public void setCustomermobileno(long customermobileno) {
		this.customermobileno = customermobileno;
	}

	public String getCustomercomments() {
		return customercomments;
	}

	public void setCustomercomments(String customercomments) {
		this.customercomments = customercomments;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}
}
