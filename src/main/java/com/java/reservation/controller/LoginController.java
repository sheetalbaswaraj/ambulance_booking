package com.java.reservation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.reservation.service.LoginService;
import com.java.reservation.util.Response;

@RestController
@CrossOrigin
public class LoginController {
	@Autowired
	private LoginService loginService;

	@PostMapping("/verifyCredentials/{mobileno}/{password}")
	public Response checkBmLoginCredentials(@PathVariable long mobileno, @PathVariable String password) {
		return loginService.fetchLoginDetailes(mobileno, password);

	}
}
