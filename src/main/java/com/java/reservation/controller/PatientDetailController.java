package com.java.reservation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.java.reservation.entity.PatientDetails;
import com.java.reservation.service.PatientDetailService;
import com.java.reservation.util.Response;

@RestController
@CrossOrigin
public class PatientDetailController {
	
	@Autowired
	private PatientDetailService Patientservice;
	
	
	@PostMapping(value ="/updateOrSavePatientData")
	public PatientDetails updateDataOrSave(@RequestBody PatientDetails patientdata) throws Exception {
		return Patientservice.updateOrsave(patientdata);
	}
	@PostMapping(value ="/DeletePatientData/{patientname}")
	public Response delete(@PathVariable("patientname") String patientname) {
		return Patientservice.deleteId(patientname);
	}
}
