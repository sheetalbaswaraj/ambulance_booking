package com.java.reservation.controller;

	import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
	import org.springframework.web.bind.annotation.PostMapping;
	import org.springframework.web.bind.annotation.RequestBody;
	import org.springframework.web.bind.annotation.RestController;

	import com.java.reservation.entity.Registration;
	import com.java.reservation.service.RegistrationService;
	import com.java.reservation.util.Response;

	@RestController
	@CrossOrigin
	public class RegistrationController {
		
		@Autowired
		private RegistrationService userservice;
		
		
		@PostMapping(value ="/updateOrSaveData")
		public Registration updateDataOrSave(@RequestBody Registration registerform) throws Exception {
			return userservice.updateOrsave(registerform);
		}
		@PostMapping(value ="/deleteBymobileno/{mobileno}")
		public Response deletedBymobileNo(@PathVariable long mobileno) {
			return userservice.deleteId(mobileno);
		}

	}
