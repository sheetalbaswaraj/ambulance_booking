package com.java.reservation.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.java.reservation.entity.Otpdata;
import com.java.reservation.service.OTPService;
import com.java.reservation.util.Response;

@RestController
@CrossOrigin
public class OTPController {
	@Autowired
	private OTPService otpservice;

	@PostMapping("/verifyOtp/{mobileno}/{otp}/{sessionkey}")
	public Response verifyotp(@PathVariable long mobileno, @PathVariable String otp,@PathVariable String sessionkey) {
		return otpservice.fetchOTPdetails(mobileno, otp, sessionkey);

	}
	
	@PostMapping("/saveOtp")
	public Response saveOtp(@RequestBody Otpdata otpverificationData) {
		return otpservice.saveData(otpverificationData);

	}
	@GetMapping("/getOTPList")
	public List<Otpdata> getOtpList(){
		return otpservice.getOTPList();
}
}
