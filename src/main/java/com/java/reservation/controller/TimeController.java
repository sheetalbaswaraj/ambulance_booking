package com.java.reservation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.java.reservation.entity.ShowtimeEntity;
import com.java.reservation.service.ShowtimeService;
import com.java.reservation.util.Response;

import RecordNotFoundException.RecordNotFoundException;


@RestController
@CrossOrigin
public class TimeController {
	
	@Autowired
	private ShowtimeService showtimeservice;
	
	@PostMapping("/savetime")
	public Response savefeedback(@RequestBody ShowtimeEntity timedata) {
		return showtimeservice.saveData(timedata);

	}
	@GetMapping("/getFarelist/{latitude}/{longitude}")
	public ShowtimeEntity getData(@PathVariable String latitude,@PathVariable String longitude) throws RecordNotFoundException {
		return showtimeservice.getdata(latitude, longitude);
	}

}
