package com.java.reservation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.java.reservation.entity.Feedbackformentity;
import com.java.reservation.service.Feedbackservice;
import com.java.reservation.util.Response;


@RestController
@CrossOrigin
public class Feedbackcontroller {
	
	@Autowired
	private Feedbackservice feedbackservice;
	
	@PostMapping("/savefeedbackdata")
	public Response savefeedback(@RequestBody Feedbackformentity feedbackdata) {
		return feedbackservice.saveData(feedbackdata);

	}

}
