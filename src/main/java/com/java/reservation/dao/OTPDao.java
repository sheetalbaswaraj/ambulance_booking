package com.java.reservation.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.java.reservation.entity.Otpdata;
import com.java.reservation.repository.OTPRepository;

@Repository
public class OTPDao {

	@Autowired
	OTPRepository otpRepository;

	public Otpdata verifyOTP(long mobileno) {

		return otpRepository.getOtp(mobileno);

	}
}
