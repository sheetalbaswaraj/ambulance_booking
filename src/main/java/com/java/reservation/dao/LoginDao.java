package com.java.reservation.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.java.reservation.entity.Registration;
import com.java.reservation.repository.LoginRepository;

@Repository
public class LoginDao {

	@Autowired
	LoginRepository loginRepository;

	public Registration verifyCredentials(long mobileno) {

		return loginRepository.gettingBmCred(mobileno);

	}
}
